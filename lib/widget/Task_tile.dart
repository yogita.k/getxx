import 'package:flutter/material.dart';

class TaskTile extends StatelessWidget {
  final bool isChecked;
  final String? taskTitle;
  final Function checkBoxState;

  const TaskTile({
    super.key,
    required this.isChecked,
    required this.taskTitle,
    required this.checkBoxState,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        shape: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
        shadowColor: Colors.blueAccent,
        elevation: 40,
        color: const Color(0xFF00008b),
        child: ListTile(
          leading: Checkbox(
            checkColor: const Color(0xFF00008b),
            fillColor: const MaterialStatePropertyAll(Colors.white),
            value: isChecked,
            onChanged: (value) {
              checkBoxState(value);
            },
          ),
          title: Text(taskTitle!,
              style: TextStyle(
                  fontSize: 25,
                  color: Colors.white,
                  decoration: isChecked ? TextDecoration.lineThrough : null)),
        ),
      ),
    );
  }
}
