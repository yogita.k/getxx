import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import '../model/detailtask.dart';

class MyDataBase {

  MyDataBase._();

  static final MyDataBase object = MyDataBase._();

   Future<Database> init() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'my.db');

    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE IF NOT EXISTS task(id INTEGER PRIMARY KEY AUTOINCREMENT , name TEXT,isDone INTEGER)');
    });
    return database;
  }

  Future<void> insertData({required Task task})async {
     Database db = await init();
    await db.insert("task",task.toMap());
    // db.rawInsert("INSERT INTO Test(id, name, isDone) VALUES(?,?,?)",[task.id,task.name,task.isDone?1:0]);
  }

  Future<void> deleteData({required int id})async {
    Database db = await init();
    await db.delete("task",where: "id = ?",whereArgs: [id]);
    //await db.rawDelete("DELETE FROM Test WHERE id = ?", [id]);
  }

  Future<List<Task>> getData()async {
    Database db = await init();
    List<Map<String, dynamic>> taskMaps  = await db.query("task",orderBy: "id");
    List<Task> task = taskMaps.map((taskMap) => Task.fromMap(taskMap)).toList();

    return task;
    //await db.rawQuery("SELECT * FROM task");
  }

  Future<void> updateData({required int id,required bool isDone})async {
     Database db = await init();
     await db.update("task", {"isDone" : isDone?1:0},where: "id = ?",whereArgs: [id]);
     //await db.rawUpdate("UPDATE task SET idDone = ?  WHERE id = ?",[id]);
  }

}
