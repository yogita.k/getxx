import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:provider/provider.dart';
import '../vm/task_data.dart';
import 'Task_tile.dart';

class TaskList extends StatefulWidget {
   TaskList({super.key});

  @override
  State<TaskList> createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  final TaskData controller= Get.put(TaskData());
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: ListView.builder(
        itemCount: controller.taskCount,
        itemBuilder: (context, index) {
          final task = controller.tasks[index];
          return Dismissible(
              key: UniqueKey(),
              direction: DismissDirection.endToStart,
              onDismissed: (_) {
                controller.deleteTask(id: task.id!);
                // taskData.deleteTask(id: task.id!);
              },
              background: Container(
                  alignment: Alignment.centerRight,
                  color: Colors.grey,
                  child: const Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Icon(
                      Icons.delete,
                      size: 30,
                    ),
                  )),
              child: TaskTile(
                isChecked: task.isDone,
                taskTitle: task.name,
                checkBoxState: (bool? checkBoxState) async {
                  controller.updateTask(task);
                  // await taskData.updateTask(task);
                },
              ));
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    final TaskData controller= Get.put(TaskData());
    controller.getData();
  }
}
