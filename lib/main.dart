
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_x/screen/TaskScreen.dart';

void main(){
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(home: TaskScreen(),);
  }
}
